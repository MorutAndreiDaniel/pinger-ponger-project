# Pinger-ponger Project
```
This project consists of two parts:
    1. First part contains 3 directories, one for pinger, one for ponger and one for kubernetes manifests. Both pinger and ponger directories have, besides go files, a built binary and a Dockerfile which can be used to create a docker image using local files.
    The steps to follow to create images that are used fo containers which will run in a kubernetes environment:
        - from pinger dir
        - docker build .
        - docker tag [image hash] [docker namespace]/[docker repository]:pinger
        - docker push [docker namespace]/[docker repository]:pinger
        - from ponger dir
        - docker build .
        - docker tag [image hash] [docker namespace]/[docker repository]:ponger
        - docker push [docker namespace]/[docker repository]:ponger
        - I have created a ping namespace so that my resources to be namespaced
        - then from kubernetes_manifests directory apply deployment.yaml: kubectl apply -f deployment.yaml
        - this will create a multi-container pod, one container for pinger and one for ponger
    2. Second part contains 4 directories, one for pinger, one for ponger, one for kubernetes manifests and one for tls. Same as in the first part, pinger and ponger directories have, besides go files, a built binary and a Dockerfile which can be used to create a docker image using local files, but I have also modified config.yaml in both places. For pinger I have updated the url to https and added 2 new attributes: "protocol: https" and "acceptCert: /app/tls/tls.crt". For ponger I have added 3 new attributes: "protocol: https", "tlsCertificate: /app/tls/tls.crt" and "tlsPrivateKey: /app/tls/tls.key". 
        Also I have modified deployment.yaml, by adding a volume for secret and at pod spec I have added volume mounts for the secret that contains a tls certificate.
        In tls directory is a script for creating a certificate and an .env file containg variables used in the script. The script contains steps for creating a certificate that will be used for encrypting communication between containers.
    The steps for creating the flow for this project:
        - as in the first part, I have created new docker images for pinger and ponger containing the updated code, and after I have tagged the images I have pushed them in docker hub
        - the next step would be to run the script from tls dir, this will create the certificate that will be stored in a secret and a ca.pem that will be stored in a config map(this is needed for the root to verify the serving certificate)
        - the last step will be to apply the deployment from kubernetes_manifests: kubectl apply -f deployment.yaml


