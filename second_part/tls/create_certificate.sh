#!/bin/bash

#Variables from .env file
source "$PWD/variables.env"

#Generate a private key and certificate signing request(CSR)

cat <<EOF | cfssl genkey - | cfssljson -bare server
{
  "hosts": [
    "$HOST"
  ],
  "CN": "$HOST",
  "key": {
    "algo": "ecdsa",
    "size": 256
  }
}
EOF
echo "Private key and certificate signing request generated"

#Generate a CSR manifest (in YAML), and send it to the API server
cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: "$HOST"
  namespace: "$NAMESPACE"
spec:
  request: $(cat server.csr | base64 | tr -d '\n')
  signerName: "kubernetes.io/$SIGNER_NAME"
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF
echo "CSR generated"
 
#Approve a certificate request
kubectl certificate approve "$HOST"  -n "$NAMESPACE"

#Create certificate authority 
cat <<EOF | cfssl gencert -initca - | cfssljson -bare ca
{
  "CN": "Custom CA Signer",
  "key": {
    "algo": "rsa",
    "size": 2048
  }
}
EOF
echo "Certificate authority created"

#Signing configuration
cat <<'EOF' > server-signing-config.json
{
    "signing": {
        "default": {
            "usages": [
                "digital signature",
                "key encipherment",
                "server auth"
            ],
            "expiry": "876000h",
            "ca_constraint": {
                "is_ca": false
            }
        }
    }
}
EOF


#Use signing configuration and the certificate authority key file and certificate to sign the certificate request
kubectl get csr "$HOST" -n "$NAMESPACE"  -o jsonpath='{.spec.request}' | \
  base64 --decode | \
  cfssl sign -ca ca.pem -ca-key ca-key.pem -config server-signing-config.json - | \
  cfssljson -bare ca-signed-server
echo "Certification request signed"

#Populate the signed certificate in the API object's status
kubectl get csr "$HOST" -n "$NAMESPACE" -o json | \
  jq '.status.certificate = "'$(base64 ca-signed-server.pem | tr -d '\n')'"' | \
  kubectl replace --raw /apis/certificates.k8s.io/v1/certificatesigningrequests/"$HOST"/status -f -
echo "Signed certificate set in API status"


#Download the issued certificate and save it to a server.crt file
kubectl get csr "$HOST" -n "$NAMESPACE" -o jsonpath='{.status.certificate}' \
    | base64 --decode > server.crt
echo "Downloaded the certificate to a .crt file"

#Populate server.crt and server-key.pem in a Secret that can later be  mounted into a Pod
kubectl create secret tls server-tls -n "$NAMESPACE"  --cert server.crt --key server-key.pem

#Populate ca.pem into a ConfigMap and use it as the trust root to verify the serving certificate
kubectl create configmap ca-configmap -n "$NAMESPACE"  --from-file ca.crt=ca.pem
