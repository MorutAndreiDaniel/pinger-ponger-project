module github.com/vodafone/sre-recruitment-assignment

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/spf13/viper v1.3.1
)

go 1.13
